import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {WooService} from "../../providers/WooService";
import {Config} from "../../providers/config";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  constructor(public navCtrl: NavController, public wooService: WooService,public config: Config) {
    this.wooService.getOrders();
    const body = {
      "order": {
        "status" : "processing"
      }
    };
    // this.wooService.updateOrder(42,body);
    this.wooService.getAllCustomers();
    // let accessToken = this.wooService.getCookie("","");
    // this.wooService.deleteOrder(42);
  }

}
