import { Config } from './config'
import {Injectable} from "@angular/core";
import { Http } from '@angular/http';
import 'rxjs/add/observable/from';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class WooService {

  Woocommerce: any;
  constructor(public config: Config,private http: Http) {
    this.Woocommerce = this.config.Woocommerce;
  }
  getOrders(){
    this.Woocommerce.getAsync("orders?customer=2").then((data) => {
      console.log(JSON.parse(data.body));
    },(err) => {
      console.log(err)
    });
  }

  updateOrder(orderNo,body){
    this.Woocommerce.put('orders/'+orderNo, body, function(err, data, res) {
      console.log(JSON.parse(data.body));
    });
  }

  deleteOrder(orderNo){
    this.Woocommerce.delete('orders/'+orderNo, function(err, data, res) {
      console.log(JSON.parse(data.body));
    });
  }

  getAllCustomers(){
    this.Woocommerce.getAsync("customers").then((data) => {
      console.log(JSON.parse(data.body));
    },(err) => {
      console.log(err)
    });
  }


  getCookie(username, password) {
    return this.http.get('http://localhost/api/auth/generate_auth_cookie/?insecure=cool&username='+username+'&password='+password).subscribe(
      response => console.log(response.json())
    );
  }

}
